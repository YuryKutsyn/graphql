const graphql = require('graphql')

const { GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLBoolean
} = graphql

const Movies = require('../models/movies')
const Directors = require('../models/directors')

const MovieType = new GraphQLObjectType({
    name: 'Movie',
    fields: ()=> ({
        id: { type: GraphQLID },
        name: { type: new GraphQLNonNull(GraphQLString) },
        genre: { type: new GraphQLNonNull(GraphQLString) },
        watched: { type: new GraphQLNonNull(GraphQLBoolean) },
        rate: { type: GraphQLInt },
        director: {
            type: DirectorType,
            resolve({ directorId }, args) {
                return Directors.findById(directorId)
            }
        }
    })
})

const DirectorType = new GraphQLObjectType({
    name: 'Director',
    fields: ()=> ({
        id: { type: GraphQLID },
        name: { type: new GraphQLNonNull(GraphQLString) },
        age: { type: new GraphQLNonNull(GraphQLInt) },
        movies: {
            type: new GraphQLList(MovieType),
            resolve({ id }, args) {
                return Movies.find({directorId: id})
            }
        }
    })
})

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addDirector: {
            type: DirectorType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLInt) }
            },
            resolve(parent, { name, age }) {
                const director = new Directors({
                    name: name,
                    age: age
                })
               return director.save()
            }
        },
        addMovie: {
            type: MovieType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                genre: { type: new GraphQLNonNull(GraphQLString) },
                watched: { type: new GraphQLNonNull(GraphQLBoolean) },
                rate: { type: GraphQLInt },
                directorId: { type: GraphQLID }
            },
            resolve(parent, { name, genre, rate, watched, directorId }) {
                const movie = new Movies({
                    name: name,
                    genre: genre,
                    rate: rate,
                    watched: watched,
                    directorId: directorId
                })
                return movie.save()
            }
        },
        deleteDirector: {
            type: DirectorType,
            args: { id: {type: GraphQLID} },
            resolve(parent, { id }) {
                return Directors.findByIdAndRemove(id)
            }
        },
        deleteMovie: {
            type: MovieType,
            args: { id: {type: GraphQLID} },
            resolve(parent, { id }) {
                return Movies.findByIdAndRemove(id)
            }
        },
        updateDirector: {
            type: DirectorType,
            args: {
                id: {type: GraphQLID},
                name: {type: GraphQLString},
                age: {type: GraphQLInt}
                },
            resolve(parent, { name, age, id }) {
               return Directors.findByIdAndUpdate(
                    id,
                    { $set: { name: name, age: age } },
                    { new: true }
                )
            }
        },
        updateMovie: {
            type: MovieType,
            args: {
                id: {type: GraphQLID},
                name: {type: new GraphQLNonNull(GraphQLString)},
                genre: {type: new GraphQLNonNull(GraphQLString)},
                watched: {type: new GraphQLNonNull(GraphQLBoolean)},
                directorId: {type: GraphQLID},
                rate: {type: GraphQLInt},
            },
            resolve(parent, { id, name, genre, directorId, watched, rate  }) {
                return Movies.findByIdAndUpdate(
                    id,
                    { $set: { name: name, genre: genre, directorId: directorId, watched: watched, rate: rate } },
                    { new: true }
                )
            }
        },
    }
})

const Query = new GraphQLObjectType({
    name: "Query",
    fields: {
        movie: {
            type: MovieType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return Movies.findById(args.id)
            }
        },
        director: {
            type: DirectorType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return Directors.findById(args.id)
            }
        },
        movies: {
            type: new GraphQLList(MovieType),
            resolve(parent, args) {
                return Movies.find({})
            }
        },
        directors: {
            type: new GraphQLList(DirectorType),
            resolve(parent, args) {
                return Directors.find({})
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutation
})