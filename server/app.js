const express = require('express')
const {graphqlHTTP} = require('express-graphql')
const schema = require('./schema/schema')
const mongoose = require('mongoose')
const cors = require('cors')

const app = express()
const PORT = 3005

const DbName = 'graphql-tutorial-yury'
mongoose.connect(`mongodb+srv://Yury:123123123@graphql-tutorial-yury.nguek.mongodb.net/${DbName}?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })

app.use(cors())

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))


app.get('/', (req,res) => {


    res.json('Hello from node js')
})

const dbConnection = mongoose.connection

dbConnection.on('error', err => console.log('Connection error: ' + err))
dbConnection.once('open', () => console.log('Connection to DB: ' + DbName))

app.listen(PORT, err => {
    err ? console.log(err) : console.log('Server started on port: ' + PORT)
})